package com.tcb.tree.identifiable;

import java.io.Serializable;

public abstract class SuidObject implements Identifiable,Serializable,Comparable<SuidObject> {
	private static final long serialVersionUID = 1L;
	private final Long suid;

	public SuidObject(Long suid){
		this.suid = suid;
	}
	
	public Long getSuid(){
		return suid;
	}
	
	@Override
	public boolean equals(Object other){
		if(this==other) return true;
		if(other==null) return false;
		if(!this.getClass().equals(other.getClass())) return false;
		SuidObject o = (SuidObject)other;
		if(getSuid().equals(o.getSuid())) return true;
		return false;
	}
	
	@Override
	public int hashCode(){
		return Long.hashCode(suid);
	}
	
	@Override
	public int compareTo(SuidObject o){
		return suid.compareTo(o.getSuid());
	}
}
