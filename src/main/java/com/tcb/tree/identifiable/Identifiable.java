package com.tcb.tree.identifiable;

import java.io.Serializable;

public interface Identifiable {
	public Long getSuid();
}
