package com.tcb.tree.identifiable;

public class SuidFactory {
	private static long nextSuid = 1l;
	
	public static synchronized long nextSuid(){
		Long suid = nextSuid;
		nextSuid++;
		return suid;
	}
}
