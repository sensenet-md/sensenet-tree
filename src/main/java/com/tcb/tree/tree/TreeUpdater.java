package com.tcb.tree.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tcb.tree.edge.Edge;
import com.tcb.tree.edge.EdgeImpl;
import com.tcb.tree.node.Node;
import com.tcb.tree.node.NodeImpl;

public class TreeUpdater {
	
	private Map<Long, Long> suidMap;

	public TreeUpdater(Map<Long,Long> suidMap){
		this.suidMap = suidMap;
	}
	
	public Tree updateSuids(Tree tree){
		List<Node> updatedNodes = new ArrayList<>();
		List<Node> updatedParents = new ArrayList<>();
		
		Node updatedRoot = null;
		for(Node n: tree.getNodes()){
			Node updated = updateNode(n);
			if(tree.getParent(n)==null) {
				updatedRoot = updated;
			} else {
				Node updatedParent = updateNode(tree.getParent(n));
				updatedNodes.add(updated);
				updatedParents.add(updatedParent);
			}                    
		}
		
		Tree updatedTree = TreeImpl.create(updatedRoot);
		
		for(int i=0;i<updatedNodes.size();i++){
			updatedTree.addNode(updatedNodes.get(i), updatedParents.get(i));
		}
				
		List<Edge> updatedEdges = new ArrayList<>();
		
		for(Edge e: tree.getEdges()){
			Edge updated = updateEdge(e,updatedTree);
			updatedEdges.add(updated);
		}
		
		if(updatedRoot==null) throw new IllegalArgumentException("No root found in tree");
				
		for(Edge edge:updatedEdges){
			updatedTree.addEdge(edge);
		}
		
		return updatedTree;
	}
	
	private Node updateNode(Node node){
		Long oldSuid = node.getSuid();
		checkHasSuid(oldSuid);
		return NodeImpl.create(suidMap.get(oldSuid));
	}
	
	private Edge updateEdge(Edge edge, Tree tree){
		Long oldSuid = edge.getSuid();
		checkHasSuid(oldSuid);
		Edge updated = EdgeImpl.create(
				suidMap.get(oldSuid),
				tree.getNode(suidMap.get(edge.getSource().getSuid())),
				tree.getNode(suidMap.get(edge.getTarget().getSuid())));
		return updated;
	}
	
	private void checkHasSuid(Long suid){
		if(!suidMap.containsKey(suid)) throw new IllegalArgumentException("Suid not found: " + suid.toString());
	}
}
