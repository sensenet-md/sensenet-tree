package com.tcb.tree.tree;

import java.util.ArrayList;
import java.util.List;

import com.tcb.tree.node.Node;

public class LeafNodeTreeSearcher {
	public static List<Node> getLeafNodes(Node parent, Tree tree){
		List<Node> nodes = new ArrayList<>();
		addLeafNodes(parent, tree, nodes);
		return nodes;
	}
	
	private static void addLeafNodes(Node currentNode, Tree tree, List<Node> buffer){
		List<Node> children = tree.getChildren(currentNode);
		if(children.isEmpty()){
			buffer.add(currentNode);
			return;
		}
		for(Node n:children){
			addLeafNodes(n,tree,buffer);
		}
	}
}
