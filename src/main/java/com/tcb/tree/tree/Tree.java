package com.tcb.tree.tree;

import java.io.Serializable;
import java.util.List;

import com.tcb.tree.edge.Edge;
import com.tcb.tree.node.Node;

public interface Tree  {
	
	public Boolean containsNode(Node node);
	public Boolean containsEdge(Edge edge);
	public void addNode(Node node, Node parent);
	public void addEdge(Edge edge);
	public Node mergeNodes(Node a, Node b);
	public Node getRoot();
	public Node getNode(Long suid);
	public Edge getEdge(Long suid);
	public List<Node> getChildren(Node node);
	public Node getParent(Node node);
	public List<Node> getNodes();
	public List<Edge> getEdges();
	public List<Edge> getConnectingEdges(Node source, Node target, Boolean directed);
	public List<Edge> getConnectingEdges(Node source, Node target);
	public List<Edge> getConnectingEdges(Node node);
	public List<Node> getSubnodes(Node node);
	public List<Edge> getConnectingSubedges(Node source, Node target);
	public List<Node> getMetanodes();
	public List<Edge> getMetaedges();
}