package com.tcb.tree.tree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.tcb.tree.edge.Edge;
import com.tcb.tree.node.Node;
import com.tcb.tree.node.NodeFactory;

import gnu.trove.impl.hash.THash;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TLongLongHashMap;
import gnu.trove.map.hash.TLongObjectHashMap;

public final class TreeImpl implements Tree, Serializable {
	private static final long serialVersionUID = 1L;
	
	private LinkedHashMap<Long,Node> nodes = new LinkedHashMap<>();
	private LinkedHashMap<Long,Edge> edges = new LinkedHashMap<>();
	private TLongObjectMap<Node> parentsMap = new TLongObjectHashMap<>();
	private TLongObjectMap<List<Node>> childrenMap = new TLongObjectHashMap<>();
	private TLongObjectMap<TLongObjectMap<List<Edge>>> edgeMap = new TLongObjectHashMap<>();
	
	private Node root;
	
	public static Tree create(Node root){
		return new TreeImpl(root);
	}
	
	private TreeImpl(Node root){
		this.root = root;
		initNode(root);
	}
	
	@Override
	public synchronized void addNode(Node node, Node parent){
		if(containsNode(node)) throw new IllegalArgumentException("Node already part of tree: " + node.toString());
		if(!containsNode(parent)) throw new IllegalArgumentException("Parent node must be part of tree: " + parent.toString());
		initNode(node);
		setParent(node,parent);
		addChild(node,parent);
	}
	
	@Override
	public Boolean containsNode(Node node){
		return nodes.containsKey(node.getSuid());
	}
	
	@Override
	public Boolean containsEdge(Edge edge){
		return edges.containsKey(edge.getSuid());
	}
	
	private synchronized void initNode(Node node){
		long suid = node.getSuid();
		nodes.put(suid,node);
		childrenMap.put(suid, new ArrayList<>());
		edgeMap.put(suid, new TLongObjectHashMap<>());
	}
				
	private synchronized void addChild(Node child, Node parent){
		long parentSuid = parent.getSuid();
		List<Node> children = childrenMap.get(parentSuid);
		children.add(child);
	}
	
	private synchronized void removeChild(Node child, Node parent){
		long parentSuid = parent.getSuid();
		List<Node> children = childrenMap.get(parentSuid);
		children.remove(child);
	}
	
	private synchronized void setParent(Node child, Node parent){
		parentsMap.put(child.getSuid(), parent);
	}
	
	@Override
	public synchronized void addEdge(Edge edge){
		checkContainsNodes(edge);
		edges.put(edge.getSuid(), edge);
		
		Node source = edge.getSource();
		Node target = edge.getTarget();
		TLongObjectMap<List<Edge>> subMap = edgeMap.get(source.getSuid());
		
		Long targetSuid = target.getSuid();
		subMap.putIfAbsent(targetSuid, new ArrayList<>());
		subMap.get(targetSuid).add(edge);
	}
	
	private void checkContainsNodes(Edge edge){
		Node source = edge.getSource();
		Node target = edge.getTarget();
		if(!containsNode(source)) 
			throw new IllegalArgumentException(
					String.format("Edge source must be part of tree: %s,%s",
							edge.toString(),source.toString()));
		if(!containsNode(target)) 
			throw new IllegalArgumentException(
					String.format("Edge target must be part of tree: %s,%s",
							edge.toString(),source.toString()));
	}
	
	@Override
	public Node getRoot(){
		return root;
	}
	
	@Override
	public Node getNode(Long suid){
		checkContainsNode(suid);
		return nodes.get(suid);
	}
	
	private void checkContainsNode(Long suid){
		if(!nodes.containsKey(suid)) throw new IllegalArgumentException("Tree does not contain node: " + suid.toString());
	}
	
	private void checkContainsNode(Node node){
		checkContainsNode(node.getSuid());
	}
	
	@Override
	public Edge getEdge(Long suid){
		checkContainsEdge(suid);
		return edges.get(suid);
	}
	
	private void checkContainsEdge(Long suid){
		if(!edges.containsKey(suid)) throw new IllegalArgumentException("Tree does not contain edge: " + suid.toString());
	}
	
	private void checkContainsEdge(Edge edge){
		checkContainsEdge(edge.getSuid());
	}
	
	@Override
	public List<Node> getChildren(Node node){
		checkContainsNode(node);
		return ImmutableList.copyOf(childrenMap.get(node.getSuid()));
	}
	
	@Override
	public Node getParent(Node node){
		checkContainsNode(node);
		long suid = node.getSuid();
		return parentsMap.get(suid);
	}
	
	@Override
	public List<Node> getNodes(){
		return ImmutableList.copyOf(nodes.values());
	}
	
	@Override
	public List<Edge> getEdges(){
		return ImmutableList.copyOf(edges.values());
	}
	
	@Override
	public List<Edge> getConnectingEdges(Node source, Node target, Boolean directed){
		checkContainsNode(source);
		checkContainsNode(target);
		
		ImmutableList.Builder<Edge> result = ImmutableList.builder();
		List<Edge> sourceEdges = edgeMap.get(source.getSuid()).get(target.getSuid());
		if(sourceEdges==null) sourceEdges = ImmutableList.of();
		result.addAll(sourceEdges);
		
		if(!directed) {
			List<Edge> targetEdges = edgeMap.get(target.getSuid()).get(source.getSuid());
			if(targetEdges==null) targetEdges = ImmutableList.of();
			result.addAll(targetEdges);
		}
		return result.build();
	}
	
	@Override
	public List<Edge> getConnectingEdges(Node source, Node target){
		return getConnectingEdges(source,target,false);
	}
	
	@Override
	public List<Edge> getConnectingEdges(Node node){
		checkContainsNode(node);
		
		ImmutableList.Builder<Edge> result = ImmutableList.builder();
		
		for(Edge edge:edges.values()){
			if(edge.getSource().equals(node) || edge.getTarget().equals(node)) {
				result.add(edge);
			}
		}
		return result.build();
	}
	
	@Override
	public List<Node> getSubnodes(Node node){
		checkContainsNode(node);
		List<Node> children = getChildren(node);
		List<Node> result = new ArrayList<>();
		for(Node child:children){
			result.add(child);
			result.addAll(getSubnodes(child));
		}
		return ImmutableList.copyOf(result);
	}
		
	@Override
	public List<Edge> getConnectingSubedges(Node source, Node target){
		List<Edge> result = new ArrayList<>();
		
		List<Node> sourceChildren = new ArrayList<>(getSubnodes(source));
		List<Node> targetChildren = new ArrayList<>(getSubnodes(target));
		
		sourceChildren.add(source);
		targetChildren.add(target);
				
		for(Node sourceChild: sourceChildren){
			for(Node targetChild: targetChildren) {
				if(isMetanode(sourceChild) || isMetanode(targetChild)) continue;
				result.addAll(getConnectingEdges(sourceChild,targetChild));
			}
		}
		return ImmutableList.copyOf(result);
	}
	
	@Override
	public List<Node> getMetanodes(){
		return getNodes().stream()
				.filter(n -> isMetanode(n))
				.collect(ImmutableList.toImmutableList());
	}
	
	@Override
	public List<Edge> getMetaedges(){
		return getEdges().stream()
				.filter(e -> isMetanode(e.getSource()) || isMetanode(e.getTarget()))
				.collect(ImmutableList.toImmutableList());
	}
	
	private Boolean isMetanode(Node n){
		return !getChildren(n).isEmpty();
	}

	@Override
	public synchronized Node mergeNodes(Node a, Node b) {
		checkContainsNode(a);
		checkContainsNode(b);
		Node parentA = getParent(a);
		Node parentB = getParent(b);
		if(parentA!=parentB)
			throw new IllegalArgumentException("Merged nodes must have the same parent");
		Node parent = parentA;
		removeChild(a, parent);
		removeChild(b, parent);
		Node n = NodeFactory.create();
		addNode(n,parent);
		setParent(a,n);
		setParent(b,n);
		addChild(a,n);
		addChild(b,n);
		return n;
	}
		
}
