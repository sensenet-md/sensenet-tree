package com.tcb.tree.node;

import com.tcb.tree.identifiable.SuidFactory;

public class NodeFactory {
	public static Node create(){
		return NodeImpl.create(SuidFactory.nextSuid());
	}
}
