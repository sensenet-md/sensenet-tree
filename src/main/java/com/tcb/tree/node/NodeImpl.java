package com.tcb.tree.node;

import java.io.Serializable;

import com.tcb.tree.identifiable.SuidObject;

public final class NodeImpl extends SuidObject implements Node,Serializable {
	private static final long serialVersionUID = 1L;
	
	public static NodeImpl create(Long suid){
		return new NodeImpl(suid);
	}
	
	public static NodeImpl createRoot(Long suid){
		return new NodeImpl(suid);
	}
	
	private NodeImpl(Long suid){
		super(suid);
	}
	
	@Override
	public String toString(){
		return "Node: " + getSuid();
	}
	
	
}
