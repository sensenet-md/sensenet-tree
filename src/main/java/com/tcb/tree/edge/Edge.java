package com.tcb.tree.edge;

import java.io.Serializable;

import com.tcb.tree.identifiable.Identifiable;
import com.tcb.tree.node.Node;

public interface Edge extends Identifiable {
	public Node getSource();
	public Node getTarget();
}
