package com.tcb.tree.edge;

import java.io.Serializable;

import com.tcb.tree.identifiable.SuidObject;
import com.tcb.tree.node.Node;

public final class EdgeImpl extends SuidObject implements Edge,Serializable {
	private static final long serialVersionUID = 1L;
	
	private final Node source;
	private final Node target;

	public static EdgeImpl create(Long suid, Node source, Node target){
		return new EdgeImpl(suid,source,target);
	}
	
	private EdgeImpl(Long suid, Node source, Node target){
		super(suid);
		this.source = source;
		this.target = target;
	}
	
	@Override
	public Node getSource(){
		return source;
	}
	
	@Override
	public Node getTarget(){
		return target;
	}
	
	@Override
	public String toString(){
		return "Edge: " + getSuid();
	}
}
