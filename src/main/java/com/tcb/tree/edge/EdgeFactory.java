package com.tcb.tree.edge;

import com.tcb.tree.identifiable.SuidFactory;
import com.tcb.tree.node.Node;

public class EdgeFactory {
	public static Edge create(Node source, Node target){
		return EdgeImpl.create(SuidFactory.nextSuid(), source, target);
	}
}
