package tree;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.tcb.common.util.SafeMap;
import com.tcb.tree.edge.Edge;
import com.tcb.tree.node.Node;
import com.tcb.tree.tree.Tree;
import com.tcb.tree.tree.TreeUpdater;

public class TreeUpdaterTest {

	private TestTree testTree;
	
	private List<Long> oldNodeSuids;
	private List<Long> oldEdgeSuids;
	
	private Map<Long,Long> suidMap;
	
	private Integer suidOffset = 2;

	private TreeUpdater updater;
	
	@Before
	public void setUp() throws Exception {
		testTree = new TestTree();
		oldNodeSuids = testTree.tree.getNodes().stream()
				.map(n -> n.getSuid())
				.collect(Collectors.toList());
		oldEdgeSuids = testTree.tree.getEdges().stream()
				.map(n -> n.getSuid())
				.collect(Collectors.toList());
		suidMap = new SafeMap<>();
		
		for(Long suid:oldNodeSuids){
			Long newSuid = suid + suidOffset;
			suidMap.put(suid, newSuid);
		}
		for(Long suid:oldEdgeSuids){
			Long newSuid = suid + suidOffset;
			suidMap.put(suid, newSuid);
		}
		
		updater = new TreeUpdater(suidMap);
	}

	@Test
	public void testUpdateSuids() {
		Tree tree = updater.updateSuids(testTree.tree);
		List<Node> nodes = tree.getNodes();
		List<Edge> edges = tree.getEdges();
		for(int i=0;i<oldNodeSuids.size();i++){
			Long suid = oldNodeSuids.get(i) + suidOffset;
			assertEquals(suid, nodes.get(i).getSuid());
		}
		for(int i=0;i<oldEdgeSuids.size();i++){
			Long suid = oldEdgeSuids.get(i) + suidOffset;
			assertEquals(suid, edges.get(i).getSuid());
		}
	}

}
