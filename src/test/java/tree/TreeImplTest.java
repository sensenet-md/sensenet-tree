package tree;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.tree.edge.Edge;
import com.tcb.tree.edge.EdgeFactory;
import com.tcb.tree.edge.EdgeImpl;
import com.tcb.tree.node.Node;
import com.tcb.tree.node.NodeFactory;
import com.tcb.tree.node.NodeImpl;
import com.tcb.tree.tree.Tree;
import com.tcb.tree.tree.TreeImpl;

public class TreeImplTest {

	private Node root = NodeFactory.create();
	private Node n1 = NodeFactory.create();
	private Node n2 = NodeFactory.create();
	private Node n3 = NodeFactory.create();
	private Node n4 = NodeFactory.create();
	private Node n5 = NodeFactory.create();
	private Node n6 = NodeFactory.create();
	private Node n7 = NodeFactory.create();
	private Node n8 = NodeFactory.create();
	
	private List<Node> nodes = Arrays.asList(root,n1,n2,n3,n4,n5,n6,n7,n8);
	
	private Edge e56a = EdgeFactory.create(n5, n6);
	private Edge e56b = EdgeFactory.create(n6, n5);
	private Edge e67 = EdgeFactory.create(n6, n7);
	private Edge e68 = EdgeFactory.create(n6, n8);
	private Edge e38 = EdgeFactory.create(n3, n8);
	
	private List<Edge> edges = Arrays.asList(e56a,e56b,e67,e68,e38);
	
	private Tree tree;
	
	@Before
	public void setUp() throws Exception {
		this.tree = TreeImpl.create(root);

		tree.addNode(n1, root);
		tree.addNode(n2, root);
		tree.addNode(n3, root);
		tree.addNode(n4, n1);
		tree.addNode(n5, n4);
		tree.addNode(n6, n4);
		tree.addNode(n7, n2);
		tree.addNode(n8, n2);
		
		tree.addEdge(e56a);
		tree.addEdge(e56b);
		tree.addEdge(e67);
		tree.addEdge(e68);
		tree.addEdge(e38);		
	}

	@Test
	public void testContainsNode() {
		assertTrue(tree.containsNode(n1));
		assertTrue(tree.containsNode(root));
		
		assertFalse(tree.containsNode(NodeFactory.create()));
	}

	@Test
	public void testContainsEdge() {
		assertTrue(tree.containsEdge(e56a));
		
		assertFalse(tree.containsEdge(EdgeFactory.create(n1, n2)));
	}

	@Test
	public void testGetRoot() {
		assertEquals(root,tree.getRoot());
	}

	@Test
	public void testGetNode() {
		assertEquals(n2,tree.getNode(n2.getSuid()));
	}

	@Test
	public void testGetEdge() {
		assertEquals(e56b,tree.getEdge(e56b.getSuid()));
	}

	@Test
	public void testGetChildren() {
		assertEquals(Arrays.asList(n1,n2,n3), tree.getChildren(root));
		assertEquals(Arrays.asList(n4), tree.getChildren(n1));
	}

	@Test
	public void testGetParent() {
		assertEquals(n1, tree.getParent(n4));
		assertEquals(root, tree.getParent(n2));
		assertEquals(null, tree.getParent(root));
	}

	@Test
	public void testGetNodes() {
		assertEquals(nodes, tree.getNodes());
	}

	@Test
	public void testGetEdges() {
		assertEquals(edges, tree.getEdges());
	}

	@Test
	public void testGetConnectingEdgesNodeNodeBoolean() {
		assertEquals(Arrays.asList(e56a), tree.getConnectingEdges(n5, n6, true));
		assertEquals(Arrays.asList(e56b), tree.getConnectingEdges(n6, n5, true));
		assertEquals(Arrays.asList(e56a,e56b), tree.getConnectingEdges(n5, n6, false));
	}

	@Test
	public void testGetConnectingEdgesNodeNode() {
		assertEquals(Arrays.asList(e56a,e56b), tree.getConnectingEdges(n5, n6));
	}

	@Test
	public void testGetConnectingEdgesNode() {
		assertEquals(Arrays.asList(e56a,e56b,e67,e68), tree.getConnectingEdges(n6));
		assertEquals(Arrays.asList(), tree.getConnectingEdges(n1));
	}

	@Test
	public void testGetSubnodes() {
		assertEquals(Arrays.asList(n4,n5,n6), tree.getSubnodes(n1));
	}

	@Test
	public void testGetConnectingSubedges() {
		assertEquals(Arrays.asList(e67,e68), tree.getConnectingSubedges(n1, n2));
		assertEquals(Arrays.asList(e67,e68), tree.getConnectingSubedges(n2, n6));
	}

	@Test
	public void testGetMetanodes() { 
		assertEquals(Arrays.asList(root,n1,n2,n4), tree.getMetanodes());
	}
	
	@Test
	public void testMergeNodes(){
		assertEquals(root, tree.getParent(n1));
		assertEquals(root, tree.getParent(n2));
		assertEquals(Arrays.asList(n4), tree.getChildren(n1));
		assertEquals(Arrays.asList(n7,n8), tree.getChildren(n2));
		assertEquals(Arrays.asList(n1,n2,n3), tree.getChildren(root));
		
		Node n = tree.mergeNodes(n1, n2);
		
		assertEquals(n, tree.getParent(n1));
		assertEquals(n, tree.getParent(n2));
		assertEquals(Arrays.asList(n4), tree.getChildren(n1));
		assertEquals(Arrays.asList(n7,n8), tree.getChildren(n2));
		assertEquals(Arrays.asList(n3,n), tree.getChildren(root));
		assertEquals(root, tree.getParent(n));
		assertEquals(Arrays.asList(n1,n2), tree.getChildren(n));
	}
}
