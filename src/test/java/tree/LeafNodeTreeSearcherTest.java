package tree;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.tree.node.Node;
import com.tcb.tree.tree.LeafNodeTreeSearcher;

public class LeafNodeTreeSearcherTest {

	private TestTree testTree;

	@Before
	public void setUp() throws Exception {
		testTree = new TestTree();
	}

	@Test
	public void testGetLeafNodesRoot() {
		List<Node> test = LeafNodeTreeSearcher.getLeafNodes(testTree.root, testTree.tree);
		
		assertEquals(Arrays.asList(testTree.n5,testTree.n6,testTree.n7,testTree.n8,testTree.n3), test);
	}
	
	@Test
	public void testGetLeafNodesNode1() {
		List<Node> test = LeafNodeTreeSearcher.getLeafNodes(testTree.n1, testTree.tree);
		
		assertEquals(Arrays.asList(testTree.n5,testTree.n6), test);
	}
	
	@Test
	public void testGetLeafNodesNode2() {
		List<Node> test = LeafNodeTreeSearcher.getLeafNodes(testTree.n2, testTree.tree);
		
		assertEquals(Arrays.asList(testTree.n7, testTree.n8), test);
	}
	
	@Test
	public void testGetLeafNodesNode3() {
		List<Node> test = LeafNodeTreeSearcher.getLeafNodes(testTree.n3, testTree.tree);
		
		assertEquals(Arrays.asList(testTree.n3), test);
	}
	
	

}
