package tree;

import java.util.Arrays;
import java.util.List;

import com.tcb.tree.edge.Edge;
import com.tcb.tree.edge.EdgeFactory;
import com.tcb.tree.node.Node;
import com.tcb.tree.node.NodeFactory;
import com.tcb.tree.tree.Tree;
import com.tcb.tree.tree.TreeImpl;

public class TestTree {
	public Node root = NodeFactory.create();
	public Node n1 = NodeFactory.create();
	public Node n2 = NodeFactory.create();
	public Node n3 = NodeFactory.create();
	public Node n4 = NodeFactory.create();
	public Node n5 = NodeFactory.create();
	public Node n6 = NodeFactory.create();
	public Node n7 = NodeFactory.create();
	public Node n8 = NodeFactory.create();
	
	public List<Node> nodes = Arrays.asList(root,n1,n2,n3,n4,n5,n6,n7,n8);
	
	public Edge e56a = EdgeFactory.create(n5, n6);
	public Edge e56b = EdgeFactory.create(n6, n5);
	public Edge e67 = EdgeFactory.create(n6, n7);
	public Edge e68 = EdgeFactory.create(n6, n8);
	public Edge e38 = EdgeFactory.create(n3, n8);
	
	public List<Edge> edges = Arrays.asList(e56a,e56b,e67,e68,e38);
	
	public Tree tree = TreeImpl.create(root);
	
	public TestTree(){
		tree.addNode(n1, root);
		tree.addNode(n2, root);
		tree.addNode(n3, root);
		tree.addNode(n4, n1);
		tree.addNode(n5, n4);
		tree.addNode(n6, n4);
		tree.addNode(n7, n2);
		tree.addNode(n8, n2);
		
		tree.addEdge(e56a);
		tree.addEdge(e56b);
		tree.addEdge(e67);
		tree.addEdge(e68);
		tree.addEdge(e38);		
	}
}
