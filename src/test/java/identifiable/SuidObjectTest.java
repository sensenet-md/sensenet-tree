package identifiable;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tcb.tree.identifiable.SuidObject;
import com.tcb.tree.node.NodeImpl;

public class SuidObjectTest {

	private SuidObject obj;
	
	private class DummySuidObject extends SuidObject {
		public DummySuidObject(Long suid) {
			super(suid);
		}
	}

	@Before
	public void setUp() throws Exception {
		this.obj = new DummySuidObject(2l);
	}

	@Test
	public void testHashCode() {
		assertEquals(2,obj.hashCode());
	}

	@Test
	public void testEqualsObject() {
		assertTrue(obj.equals(obj));
		SuidObject other = new DummySuidObject(2l);
		assertTrue(obj.equals(other));
		
		NodeImpl node = NodeImpl.create(2l);
		assertFalse(obj.equals(node));
		
		other = new DummySuidObject(3l);
		assertFalse(obj.equals(other));
	}

}
